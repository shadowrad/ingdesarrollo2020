from juego.models import Personaje

def controlar_nombre(nombre):
    nombres = ['admin', 'malo']
    if nombre in nombres:
        raise Exception('Ese nombre no esta permitido')


def cargar_personaje_base(nombre,profesion_id, nivel):
    # obtengo los datos del request
    # validacion si es mayor que 99 tira error
    if int(nivel) > 99:
        raise Exception("Error personaje no puede ser mayor a 99")
    # creo el nuevo personaje
    personaje = Personaje()
    personaje.nombre = nombre
    personaje.clase_id = profesion_id
    personaje.nivel = nivel
    # el metodo save lo salva a la base de datos
    personaje.save()
