from django.db import models



class Profesion(models.Model):
    nombre = models.CharField(max_length=200)

class Habilidades(models.Model):
    nombre = models.CharField(max_length=200)
    #  related_name sirve para poder definir el como se va a llamar desde se clase relacionada,
    #  por ejemplo en este caso con rofesion.habilidades.all() se va a poder obtener todas las habilidades
    profesion = models.ForeignKey(Profesion, on_delete=models.CASCADE, related_name= 'habilidades')
    nivel_min = models.IntegerField()


class Personaje(models.Model):
    nombre = models.CharField(max_length=200, unique=True)
    nivel = models.IntegerField()
    clase = models.ForeignKey(Profesion, on_delete=models.CASCADE, related_name='personajes')