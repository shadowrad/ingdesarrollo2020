from django.db import IntegrityError
from django.shortcuts import render, redirect

# Create your views here.
from juego.helpers.cargas import cargar_personaje_base
from juego.models import Profesion, Habilidades, Personaje


def cargar_profesion(request):
    # si es por post quiere decir que viene desde el formulario
    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        profesion = Profesion()
        profesion.nombre = nombre
        profesion.save()

    # obtengo la lista entera de profesiones
    lista = Profesion.objects.all()
    return render(request, 'profesion_carga.html',{'lista':lista})


def cargar_habilidad(request):
    # si es por post quiere decir que viene desde el formulario
    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        profesion_id = request.POST.get('profesion')
        nivel = request.POST.get('nivel')
        habilidad = Habilidades()
        habilidad.nombre = nombre
        habilidad.profesion_id = profesion_id
        habilidad.nivel_min = nivel
        habilidad.save()
    # obtengo la lista entera de habilidades
    lista = Habilidades.objects.all()
    # obtengo la lista entera de profesiones
    profesiones = Profesion.objects.all()
    return render(request, 'habilidad_carga.html',{'lista':lista,'profesiones':profesiones})

def cargar_personajeSinExcept(request):
    # obtengo la lista entera de personajes
    lista = Personaje.objects.all()
    # obtengo la lista entera de profesiones
    profesiones = Profesion.objects.all()
    # si es por post quiere decir que viene desde el formulario
    if request.method == 'POST':
        # obtengo los datos del request
        nombre = request.POST.get('nombre')
        profesion_id = request.POST.get('profesion')
        nivel = request.POST.get('nivel')
        # validacion si es mayor que 99 tira error
        if int(nivel) < 1:
            error = "Error personaje no puede ser menor a 1"
            return render(request, 'personajes_carga.html', {'lista': lista, 'profesiones': profesiones, 'error':error})

        if int(nivel) > 99:
            error = "Error personaje no puede ser mayor a 99"
            return render(request, 'personajes_carga.html', {'lista': lista, 'profesiones': profesiones, 'error':error})

        nombres = ['admin', 'malo']
        if nombre in nombres:
            error = "Nombre invalido"
            return render(request, 'personajes_carga.html',
                          {'lista': lista, 'profesiones': profesiones, 'error': error})

        # creo el nuevo personaje
        personaje = Personaje()
        personaje.nombre = nombre
        personaje.clase_id = profesion_id
        personaje.nivel = nivel
        # el metodo save lo salva a la base de datos
        personaje.save()
    return render(request, 'personajes_carga.html',{'lista':lista,'profesiones':profesiones})

def cargar_personaje(request):
    # obtengo la lista entera de personajes
    lista = Personaje.objects.all()
    # obtengo la lista entera de profesiones
    profesiones = Profesion.objects.all()
    # si es por post quiere decir que viene desde el formulario
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            profesion_id = request.POST.get('profesion')
            nivel = request.POST.get('nivel')
            cargar_personaje_base(nombre, profesion_id, nivel)
        except IntegrityError as otroErr:
            err = 'El nombre no puede estar repetido'
            return render(request, 'personajes_carga.html',
                          {'lista': lista, 'profesiones': profesiones, 'error': str(err)})

        except Exception as err:
            return render(request, 'personajes_carga.html',
                          {'lista': lista, 'profesiones': profesiones, 'error': str(err)})

    return render(request, 'personajes_carga.html',{'lista':lista,'profesiones':profesiones})


def modificar_personaje(request, id_personaje):
    # antes que nada obtengo el personaje que quiero modificar
    personaje= Personaje.objects.get(id=id_personaje)
    profesiones = Profesion.objects.all()
    # si es por post quiere decir que viene desde el formulario
    if request.method == 'POST':
        # obtengo los datos del request
        nombre = request.POST.get('nombre')
        profesion_id = request.POST.get('profesion')
        nivel = request.POST.get('nivel')
        if nivel > 99:
            error = "Error personaje no puede ser mayor a 99"
            return render(request, 'personajes_modificar.html', {'profesiones': profesiones, 'personaje': personaje})
        personaje.nombre = nombre
        personaje.clase_id = profesion_id
        personaje.nivel = nivel
        personaje.save()
        # re direcciono a la pagina de los personajes
        return redirect('/juego/personaje')

    return render(request, 'personajes_modificar.html',{'profesiones':profesiones,'personaje':personaje})