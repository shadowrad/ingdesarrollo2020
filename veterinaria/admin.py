from django.contrib import admin

# Register your models here.
from veterinaria.models import Veterinario, Mascota, Duenio


class MascotaInline(admin.TabularInline):
    model = Mascota
    extra = 1

class DuenioAdmin(admin.ModelAdmin):
    inlines = [MascotaInline]


admin.site.register(Mascota)
admin.site.register(Duenio, DuenioAdmin)
admin.site.register(Veterinario)