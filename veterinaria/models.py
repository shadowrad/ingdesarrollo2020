from django.db import models
RAZAS = [('DOBERMAN','dobermas'),('OBEJERO','Objero Aleman')]

class Duenio(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    def __str__(self):
        return self.nombre

# Create your models here.
class Mascota(models.Model):
    nombre = models.CharField(max_length=100)
    raza =  models.CharField(max_length=100,choices=RAZAS )
    duenio = models.ForeignKey(Duenio, related_name='mascotas', on_delete=models.CASCADE)
    def __str__(self):
        return self.nombre

class Veterinario(models.Model):
    nombre = models.CharField(max_length=100)
    matricula = models.CharField(max_length=100)
    mascota = models.ManyToManyField(Mascota, related_name='veterinarios')

    def __str__(self):
        return self.nombre


