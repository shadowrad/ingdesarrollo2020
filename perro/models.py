
# Create your models here.
from django.db import models


class Raza(models.Model):
    descripcion = models.CharField(max_length=100)


class Perro(models.Model):
    nombre = models.CharField(max_length=100)
    raza = models.ForeignKey(Raza, on_delete=models.CASCADE, related_name='perros')




