from django.shortcuts import render

# Create your views here.
from perro.models import Perro, Raza


def cargar_perro(request):
    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        raza_id = request.POST.get('raza')
        perro = Perro()
        perro.nombre = nombre
        perro.raza = Raza.objects.get(id=raza_id)
        perro.save()

    lista = Perro.objects.all()
    lista_raza = Raza.objects.all()
    return render(request,'personajes_carga.html',{'lista':lista, 'razas':lista_raza})


def cargar_raza(request):
    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        raza = Raza()
        raza.descripcion = nombre
        raza.save()

    lista = Raza.objects.all()
    return render(request, 'raza_carga.html',{'lista':lista})


def listar_perros(request):
    lista_raza = Raza.objects.all()
    lista = Perro.objects.all()
    if request.method == 'POST':
        raza_id = request.POST.get('raza')
        if raza_id != '':
            lista = Perro.objects.filter(raza_id=raza_id)

    return render(request,'perros_lista.html',{'lista':lista, 'razas':lista_raza})
