"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from perro import views
from juego import views as view_juego

urlpatterns = [
    path('admin/', admin.site.urls),
    path('perro/', views.cargar_perro),
    path('raza/', views.cargar_raza),
    path('perro/list', views.listar_perros),
    path('juego/profesion',view_juego.cargar_profesion),
    path('juego/habilidades', view_juego.cargar_habilidad),
    path('juego/personaje', view_juego.cargar_personaje),
    path('juego/personaje/<int:id_personaje>', view_juego.modificar_personaje),

]
